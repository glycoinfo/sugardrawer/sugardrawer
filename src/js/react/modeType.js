"use strict";

export const modeType = {
    NODE: Symbol(),
    EDGE: Symbol(),
    MODIFICATION: Symbol(),
    REPEAT: Symbol(),
    FRAGMENT: Symbol(),
    STRUCTURE: Symbol(),
    COMPOSITION: Symbol(),
    DELETE: Symbol(),
    UNDO: Symbol(),
    REDO: Symbol(),
    NOT_SELECTED: Symbol(),
    UPDATE: Symbol(),
    //OUTPUT: Symbol(),
    EXPORT: Symbol(),
    IMPORT: Symbol(),
    EDIT: Symbol()
};

