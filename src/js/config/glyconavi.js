//https://glyconavi.org/hub/?id={GlyTouCanID}
//https://glyconavi.org/hub/?wurcs={WURCS}
export default {
    API_URL: "https://glyconavi.org/hub/?wurcs=",
    KEY: "WURCS",
    CAPTION: "GlycoNAVI"
};

