"use strict";

import  React from "react";
import {Button, Modal, Header} from "semantic-ui-react";

export class ToolHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        const elementMarginCanvas = {
            marginLeft: "250px",
            marginRight: "30px",
            marginTop: "10px"
        };

        return (
            <Header as={"h1"} style={elementMarginCanvas}>
                    SugarDrawer
                <Header.Subheader>
                    <Modal trigger={<Button circular icon={"info"} />}>
                        <Modal.Header>SugarDrawer: 1.8.1</Modal.Header>
                        <Modal.Content>
                            <ul>
                                <li>
                                Symbol Nomenclature for Glycans (SNFG): <a href={"https://www.ncbi.nlm.nih.gov/glycans/snfg.html"}>https://www.ncbi.nlm.nih.gov/glycans/snfg.html</a><br/><p/>
                                </li>
                                <li>
                                Shinichiro Tsuchiya, Masaaki Matsubara, Kiyoko F. Aoki-Kinoshita and Issaku Yamada, "SugarDrawer: A Web-Based Database Search Tool with Editing Glycan Structures" Molecules 26.23 (2021):7149.
                                     (<a href={"https://doi.org/10.3390/molecules26237149"}>https://doi.org/10.3390/molecules26237149</a>)
                                </li>
                            </ul>
                        </Modal.Content>
                    </Modal>
                </Header.Subheader>
            </Header>
        );
    }
}