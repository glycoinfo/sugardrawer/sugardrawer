//@flow
"use strict";

export const SNFGSymbolGlycan: Object = {
    "4eLeg": {
        shortName: "4-Epilegionaminic acid",
        systematicName: "5,7-Diamino-3,5,7,9-tetradeoxy-D-glycero-D-talo-non-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 9,
        anomericPosition: 1,
        disablePosition: [5,7]
    },
    "SixdAlt": {
        shortName: "6-Deoxy-L-altrose",
        systematicName: "6-Deoxy-L-altroP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "SixdAltNAc": {
        shortName: "N-Acetyl-6-deoxy-L-altrosamine",
        systematicName: "2-Acetamido-2,6-dideoxy-L-altroP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "SixdGul": {
        shortName: "6-Deoxy-D-gulose",
        systematicName: "6-Deoxy-D-guloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "SixdTal": {
        shortName: "6-Deoxy-D-talose",
        systematicName: "6-Deoxy-D-taloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "SixdTalNAc": {
        shortName: "N-Acetyl-6-deoxy-D-talosamine",
        systematicName: "2-Acetamido-2,6-dideoxy-D-taloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Abe": {
        shortName: "Abequose",
        systematicName: "3,6-Dideoxy-D-xylo-hexoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "Aci": {
        shortName: "Acinetaminic acid",
        systematicName: "5,7-Diamino-3,5,7,9-tetradeoxy-L-glycero-L-altro-non-2-ulopyranosonic acid",
        isomer: "LL",
        ring: "P",
        carbBone: 9,
        anomericPosition: 1,
        disablePosition: [5,7]
    },
    "All": {
        shortName: "D-Allose",
        systematicName: "D-AlloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "AllA": {
        shortName: "D-Alluronic acid",
        systematicName: "D-Allopyranuronic acid",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "AllN": {
        shortName: "D-Allosamine",
        systematicName: "2-Amino-2-deoxy-D-alloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "AllNAc": {
        shortName: "N-Acetyl-D-allosamine",
        systematicName: "2-Acetamido-2-deoxy-D-alloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Alt": {
        shortName: "L-Altrose",
        systematicName: "L-AltroP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "AltA": {
        shortName: "L-Altruronic acid",
        systematicName: "L-Altropyranuronic acid",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "AltN": {
        shortName: "L-Altrosamine",
        systematicName: "2-Amino-2-deoxy-L-altroP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "AltNAc": {
        shortName: "N-Acetyl-L-altrosamine",
        systematicName: "2-Acetamido-2-deoxy-L-altroP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Api": {
        shortName: "L-Apiose",
        systematicName: "3-C-(Hydroxymethyl)-L-erythro-tetroF",
        isomer: "L",
        ring: "F",
        carbBone: 5,
        anomericPosition: 1,
        disablePosition: [3]
    },
    "Ara": {
        shortName: "L-Arabinose",
        systematicName: "L-ArabinoP",
        isomer: "L",
        ring: "P",
        carbBone: 5,
        anomericPosition: 1,
        disablePosition: []
    },
    "Bac": {
        shortName: "Bacillosamine",
        systematicName: "2,4-Diamino-2,4,6-trideoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2,4]
    },
    "Col": {
        shortName: "Colitose",
        systematicName: "3,6-Dideoxy-L-xylo-hexoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "DDManHep": {
        shortName: "D-glycero-D-manno-Heptose",
        systematicName: "D-glycero-D-manno-HeptoP",
        isomer: "D",
        ring: "P",
        carbBone: 7,
        anomericPosition: 1,
        disablePosition: []
    },
    "Dha": {
        shortName: "3-Deoxy-D-lyxo-heptulosaric acid",
        systematicName: "3-Deoxy-D-lyxo-hept-2-ulopyranosaric acid",
        isomer: "D",
        ring: "P",
        carbBone: 7,
        anomericPosition: 1,
        disablePosition: []
    },
    "Dig": {
        shortName: "D-Digitoxose",
        systematicName: "2,6-Dideoxy-D-ribo-hexoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "Fru": {
        shortName: "D-Fructose",
        systematicName: "D-arabino-Hex-2-uloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 2,
        disablePosition: []
    },
    "Fuc": {
        shortName: "L-Fucose",
        systematicName: "6-Deoxy-L-galactoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "FucNAc": {
        shortName: "N-Acetyl-L-fucosamine",
        systematicName: "2-Acetamido-2,6-dideoxy-L-galactoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Gal": {
        shortName: "D-Galactose",
        systematicName: "D-GalactoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "GalA": {
        shortName: "D-Galacturonic acid",
        systematicName: "D-Galactopyranuronic acid",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "GalN": {
        shortName: "D-Galactosamine",
        systematicName: "2-Amino-2-deoxy-D-galactoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "GalNAc": {
        shortName: "N-Acetyl-D-galactosamine",
        systematicName: "2-Acetamido-2-deoxy-D-galactoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Glc": {
        shortName: "D-Glucose",
        systematicName: "D-GlucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "GlcA": {
        shortName: "D-Glucuronic acid",
        systematicName: "D-Glucopyranuronic acid",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "GlcN": {
        shortName: "D-Glucosamine",
        systematicName: "2-Amino-2-deoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "GlcNAc": {
        shortName: "N-Acetyl-D-glucosamine",
        systematicName: "2-Acetamido-2-deoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Gul": {
        shortName: "D-Gulose",
        systematicName: "D-GuloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "GulA": {
        shortName: "D-Guluronic acid",
        systematicName: "D-Gulopyranuronic acid",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "GulN": {
        shortName: "D-Gulosamine",
        systematicName: "2-Amino-2-deoxy-D-guloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "GulNAc": {
        shortName: "N-Acetyl-D-gulosamine",
        systematicName: "2-Acetamido-2-deoxy-D-guloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Ido": {
        shortName: "L-Idose",
        systematicName: "L-IdoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "IdoA": {
        shortName: "L-Iduronic acid",
        systematicName: "L-Idopyranuronic acid",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "IdoN": {
        shortName: "L-Idosamine",
        systematicName: "2-Amino-2-deoxy-L-idoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "IdoNAc": {
        shortName: "N-Acetyl-L-idosamine",
        systematicName: "2-Acetamido-2-deoxy-L-idoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Kdn": {
        shortName: "3-Deoxy-D-glycero-D-galacto-nonulosonic acid",
        systematicName: "3-Deoxy-D-glycero-D-galacto-non-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: []
    },
    "Kdo": {
        shortName: "3-Deoxy-D-manno-octulosonic acid",
        systematicName: "3-Deoxy-D-manno-oct-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 8,
        anomericPosition: 2,
        disablePosition: []
    },
    "Leg": {
        shortName: "Legionaminic acid",
        systematicName: "5,7-Diamino-3,5,7,9-tetradeoxy-D-glycero-D-galacto-non-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: [5,7]
    },
    "LDManHep": {
        shortName: "L-glycero-D-manno-Heptose",
        systematicName: "L-glycero-D-manno-HeptoP",
        isomer: "L",
        ring: "P",
        carbBone: 7,
        anomericPosition: 1,
        disablePosition: []
    },
    "Lyx": {
        shortName: "D-Lyxose",
        systematicName: "D-LyxoP",
        isomer: "D",
        ring: "P",
        carbBone: 5,
        anomericPosition: 1,
        disablePosition: []
    },
    "Man": {
        shortName: "D-Mannose",
        systematicName: "D-MannoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "ManA": {
        shortName: "D-Mannuronic acid",
        systematicName: "D-Mannopyranuronic acid",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "ManN": {
        shortName: "D-Mannosamine",
        systematicName: "2-Amino-2-deoxy-D-mannoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "ManNAc": {
        shortName: "N-Acetyl-D-mannosamine",
        systematicName: "2-Acetamido-2-deoxy-D-mannoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Mur": {
        shortName: "Muramic acid",
        systematicName: "2-Amino-3-O-[(R)-1-carboxyethyl]-2-deoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2,3]
    },
    "MurNAc": {
        shortName: "N-Acetylmuramic acid",
        systematicName: "2-Acetamido-3-O-[(R)-1-carboxyethyl]-2-deoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2,3]
    },
    "MurNGc": {
        shortName: "N-Glycolylmuramic acid",
        systematicName: "3-O-[(R)-1-Carboxyethyl]-2-deoxy-2-glycolamido-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2,3]
    },
    "Neu": {
        shortName: "Neuraminic acid",
        systematicName: "5-Amino-3,5-dideoxy-D-glycero-D-galacto-non-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: [5]
    },
    "Neu5Ac": {
        shortName: "N-Acetylneuraminic acid",
        systematicName: "5-Acetamido-3,5-dideoxy-D-glycero-D-galacto-non-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: [5]
    },
    "Neu5Gc": {
        shortName: "N-Glycolylneuraminic acid",
        systematicName: "3,5-Dideoxy-5-glycolamido-D-glycero-D-galacto-non-2-ulopyranosonic acid",
        isomer: "D",
        ring: "P",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: [5]
    },
    "Oli": {
        shortName: "Olivose",
        systematicName: "2,6-Dideoxy-D-arabino-hexoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "Par": {
        shortName: "Paratose",
        systematicName: "3,6-Dideoxy-D-ribo-hexoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "Pse": {
        shortName: "Pseudaminic acid",
        systematicName: "5,7-Diamino-3,5,7,9-tetradeoxy-L-glycero-L-manno-non-2-ulopyranosonic acid",
        isomer: "L",
        ring: "P",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: [5,7]
    },
    "Psi": {
        shortName: "D-Psicose",
        systematicName: "D-ribo-Hex-2-uloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 2,
        disablePosition: []
    },
    "Qui": {
        shortName: "D-Quinovose",
        systematicName: "6-Deoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "QuiNAc": {
        shortName: "N-Acetyl-D-quinovosamine",
        systematicName: "2-Acetamido-2,6-dideoxy-D-glucoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Rha": {
        shortName: "L-Rhamnose",
        systematicName: "6-Deoxy-L-mannoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "RhaNAc": {
        shortName: "N-Acetyl-L-rhamnosamine",
        systematicName: "2-Acetamido-2,6-dideoxy-L-mannoP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Rib": {
        shortName: "D-Ribose",
        systematicName: "D-RiboP",
        isomer: "D",
        ring: "P",
        carbBone: 5,
        anomericPosition: 1,
        disablePosition: []
    },
    "Sia": {
        shortName: "Sialic acid",
        systematicName: "Sialic acid residue of unspecified type",
        isomer: "D",
        ring: "undefined",
        carbBone: NaN,
        anomericPosition: 1,
        disablePosition: []
    },
    "Sor": {
        shortName: "L-Sorbose",
        systematicName: "L-xylo-Hex-2-uloP",
        isomer: "L",
        ring: "P",
        carbBone: 6,
        anomericPosition: 2,
        disablePosition: []
    },
    "Tag": {
        shortName: "D-Tagatose",
        systematicName: "D-lyxo-Hex-2-uloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 2,
        disablePosition: []
    },
    "Tal": {
        shortName: "D-Talose",
        systematicName: "D-TaloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "TalA": {
        shortName: "D-Taluronic acid",
        systematicName: "D-Talopyranuronic acid",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "TalN": {
        shortName: "D-Talosamine",
        systematicName: "2-Amino-2-deoxy-D-taloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "TalNAc": {
        shortName: "N-Acetyl-D-talosamine",
        systematicName: "2-Acetamido-2-deoxy-D-taloP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "Tyv": {
        shortName: "Tyvelose",
        systematicName: "3,6-Dideoxy-D-arabino-hexoP",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "Xyl": {
        shortName: "D-Xylose",
        systematicName: "D-XyloP",
        isomer: "D",
        ring: "P",
        carbBone: 5,
        anomericPosition: 1,
        disablePosition: []
    },
    "Hex": {
        shortName: "D-Hexose",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "HexNAc": {
        shortName: "N-Acetyl-D-Hexose",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "HexN": {
        shortName: "D-Hexosamine",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "HexA": {
        shortName: "D-Hexonic Acid",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "dHex": {
        shortName: "D-Deoxyhexoseo",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "dHexNAc": {
        shortName: "D-DeoxyhexNAc",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: [2]
    },
    "ddHex": {
        shortName: "D-Di-deoxyhexose",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 6,
        anomericPosition: 1,
        disablePosition: []
    },
    "Pen": {
        shortName: "D-Pentose",
        systematicName: "undefined",
        isomer: "D",
        ring: "P",
        carbBone: 5,
        anomericPosition: 1,
        disablePosition: []
    },
    "Nonu": {
        shortName: "Deoxynonulosonate",
        systematicName: "undefined",
        isomer: "undefined",
        ring: "undefined",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: []
    },
    "dNon": {
        shortName: "Di-deoxynonulosonate",
        systematicName: "undefined",
        isomer: "undefined",
        ring: "undefined",
        carbBone: 9,
        anomericPosition: 2,
        disablePosition: []
    },
    "Unknown": {
        shortName: "Unknown",
        systematicName: "undefined",
        isomer: "undefined",
        ring: "undefined",
        carbBone: NaN,
        anomericPosition: 1,
        disablePosition: []
    },
    "Assigned": {
        shortName: "Assigned",
        systematicName: "undefined",
        isomer: "undefined",
        ring: "undefined",
        carbBone: NaN,
        anomericPosition: 1,
        disablePosition: []
    }
};