"use strict";

import React from "react";
import {Help} from "./Help";
import {SidebarLeftPush} from "./expertUI/SidebarLeftPush";
import {modeType} from "./modeType";
import {liaise} from "../script";
import Button from "semantic-ui-react/dist/commonjs/elements/Button/Button";
import ModalList from "./simpleUI/ModalList";
import {search} from "../script/util/search";
import {helpTemplate} from "./helpTemplate";
import {Popup} from "semantic-ui-react";
import TextImporterMenu from "./underUI/TextImportMenu";
import ReactDOM from "react-dom";
import TextExportMenu from "./underUI/TextExportMenu";
import ScaleSlider from "./commonUI/ScaleSlider";
import ModeCancelButton from "./horizonalUI/ModeCancelButton";
import UpdateGlycan from "../script/images/update/UpdateGlycan";
import ModalCanvasDelete from "./commonUI/ModalCanvasDelete";

export class Interface extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            current_mode_type: modeType.NOT_SELECTED,
            explainText: "This is a message area of the selected function.",
            sideBarVisible: false,
            popUpVisible: false,
            simpleMode: false,
            visible: false,
            lastWork: ""
        };

        //this.close = this.close.bind(this);
        //this.clearCanvas = this.clearCanvas.bind(this);

        // assign side bar operation utility
        liaise.sideBarCancel = this.makeCancelUtility();
    }

    onClickEvent(e) {
        let currentState = this.state;
        const targetId = e.currentTarget.name;
        if (targetId === "node") {
            this.initFunctions();
            currentState.current_mode_type = modeType.NODE;
            currentState = this.changeVisual(currentState, targetId);
            currentState.explainText = helpTemplate(targetId);
            //this.addModeCancelButton();
        } else if (targetId === "edge") {
            this.initFunctions();
            currentState.current_mode_type = modeType.EDGE;
            currentState = this.changeVisual(currentState, targetId);
            currentState.explainText = helpTemplate(targetId);
            //this.addModeCancelButton();
        } else if (targetId === "addModification") {
            this.initFunctions();
            currentState.current_mode_type = modeType.MODIFICATION;
            currentState = this.changeVisual(currentState, targetId);
            currentState.explainText = helpTemplate(targetId);
            //this.addModeCancelButton();
        } else if (targetId === "structure") {
            this.initFunctions();
            currentState.current_mode_type = modeType.STRUCTURE;
            currentState = this.changeVisual(currentState, targetId);
            currentState.explainText = helpTemplate(targetId);
            //this.addModeCancelButton();
        } else if (targetId === "repeat") {
            currentState.current_mode_type = modeType.REPEAT;
            currentState.sideBarVisible = false;
            currentState.explainText = helpTemplate(targetId);
        } else if (targetId === "fragment") {
            this.initFunctions();
            currentState.current_mode_type = modeType.FRAGMENT;
            currentState = this.changeVisual(currentState, targetId);
            currentState.explainText = helpTemplate(targetId);
            //this.addModeCancelButton();
        } else if (targetId === "composition") {
            this.initFunctions();
            currentState.current_mode_type = modeType.COMPOSITION;
            currentState.popUpVisible = true;
            currentState.explainText = helpTemplate(targetId);
        } else if (targetId === "delete") {
            const glycan = liaise.coreGraph;
            if (!glycan || glycan.getRootNode() === undefined) {
                currentState.current_mode_type = modeType.NOT_SELECTED;
            } else {
                currentState.current_mode_type = modeType.DELETE;
            }
            currentState.sideBarVisible = false;
            currentState.explainText = helpTemplate(targetId);
        } else if (targetId === "undo") {
            this.setState({explainText: helpTemplate(targetId)});
            const glycan = liaise.coreGraph;
            if (!glycan || glycan.getRootNode() === undefined) {
                alert("Please draw any glycan on the canvas.");
                return;
            }
            liaise.actionUndoRedo.undo();
        } else if (targetId === "redo") {
            this.setState({explainText: helpTemplate(targetId)});
            const glycan = liaise.coreGraph;
            if (!glycan || glycan.getRootNode() === undefined) {
                alert("Please draw any glycan on the canvas.");
                return;
            }
            liaise.actionUndoRedo.redo();
        } else if (targetId === "help") {
            currentState.visible = (!this.state.visible);
            this.setState(currentState);
            return;
        } else if (targetId === "simple") {
            currentState.simpleMode = true;
            currentState.sideBarVisible = false;
            currentState.current_mode_type = modeType.NOT_SELECTED;
            currentState.explainText = helpTemplate(targetId);
            liaise.interfaceType = targetId;
        } else if (targetId === "expert") {
            currentState.simpleMode = false;
            currentState.popUpVisible = false;
            currentState.current_mode_type = modeType.NOT_SELECTED;
            currentState.explainText = helpTemplate(targetId);
            liaise.interfaceType = targetId;
        } else if (targetId === "search") {
            this.setState({explainText: helpTemplate(targetId)});
            const glycan = liaise.coreGraph;
            if (!glycan || glycan.getRootNode() === undefined) {
                alert("Please draw any glycan on the canvas.");
                return;
            }
            search(glycan);
        } else if (targetId === "import") {
            currentState.current_mode_type = modeType.IMPORT;
            currentState.explainText = helpTemplate(targetId);

            document.querySelector("#idtable").textContent = "";
            document.querySelector("#textArea").style.display = "block";
            ReactDOM.render(
                <TextImporterMenu/>,
                document.querySelector("#textArea")
            );
        } else if (targetId === "export") {
            currentState.current_mode_type = modeType.EXPORT;
            currentState.explainText = helpTemplate(targetId);

            document.querySelector("#idtable").textContent = "";
            document.querySelector("#textArea").style.display = "block";
            ReactDOM.render(
                <TextExportMenu/>,
                document.querySelector("#textArea")
            );
        } else if (targetId === "normalize") {
            currentState.current_mode_type = modeType.EDIT;
            currentState.explainText = helpTemplate(targetId);
            const upGraph = new UpdateGlycan();
            upGraph.updateGlycanImage();
        }

        currentState.lastWork = targetId;
        this.setState(currentState);
        liaise.modeType = currentState.current_mode_type;
    }

    render() {
        const defFunctionMenu = {
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center",
            marginTop: "10px",
            marginBottom: "10px",
            variant: "outline-dark",
            centered: true
        };

        const elementMargin = {
            style: {
                width: "60%",
                marginLeft: "auto",
                marginRight: "auto",
                marginBottom: "10px",
                display: this.state.visible ? "block" : "none"
            }
        };

        const elementMarginCanvas = {
            style: {
                marginLeft: "30px",
                marginRight: "30px"
            }
        };

        return (
            <div id={"cmdtmp"}>
                <div id={"upcmd"} style = { defFunctionMenu }>
                    <div id={"edit-glycan"}>
                        <Button color={this.state.current_mode_type === modeType.STRUCTURE ? "grey" : (this.state.simpleMode ? "teal" : "facebook")} name="structure" selected = { this.state.current_mode_type === modeType.STRUCTURE } onClick={(e) => this.onClickEvent(e)}>Load Structure</Button>
                        <Button color={this.state.current_mode_type === modeType.NODE ? "grey" : (this.state.simpleMode ? "teal" : "facebook")} name="node" selected = { this.state.current_mode_type === modeType.NODE } onClick={(e) => this.onClickEvent(e)}>Add Monosaccharide</Button>
                        <Button style={{display : this.state.simpleMode ? "block" : "none"}} color={this.state.current_mode_type === modeType.EDGE ? "grey" : (this.state.simpleMode ? "teal" : "facebook")} name="edge" selected = { this.state.current_mode_type === modeType.EDGE } onClick={(e) => this.onClickEvent(e)}>Add Linkage</Button>
                        <Button color={this.state.current_mode_type === modeType.MODIFICATION ? "grey" : (this.state.simpleMode ? "teal" : "facebook")} name="addModification" selected = { this.state.current_mode_type === modeType.MODIFICATION } onClick={(e) => this.onClickEvent(e)}>Add Substituent</Button>
                        <Button style={{display: "none"}} color={"facebook"} name="repeat" selected = { this.state.current_mode_type === modeType.REPEAT} onClick={(e) => this.onClickEvent(e)}>Draw repeating unit</Button>
                        <Button color={this.state.current_mode_type === modeType.FRAGMENT ? "grey" : (this.state.simpleMode ? "teal" : "facebook")} name="fragment" selected = { this.state.current_mode_type === modeType.FRAGMENT} onClick={(e) => this.onClickEvent(e)}>Add Fragment</Button>
                        <Button style={{display: "none"}} color={"facebook"} name="composition" selected = { this.state.current_mode_type === modeType.COMPOSITION} onClick={(e) => this.onClickEvent(e)}>Add composition</Button>
                    </div>
                    <div id={"edit-util"} style={{marginLeft: "15px"}}>
                        <Popup on={"hover"} content='Undo' trigger={
                            <Button icon="reply" color={"google plus"} name="undo" selected = { this.state.current_mode_type === modeType.UNDO} onClick={(e) => this.onClickEvent(e)} />
                        } />
                        <Popup on={"hover"} content='Redo' trigger={
                            <Button icon="share" color={"google plus"} name="redo" selected = { this.state.current_mode_type === modeType.REDO} onClick={(e) => this.onClickEvent(e)} />
                        } />
                        <Popup on={"hover"} content='Delete' trigger={
                            <Button icon="delete" color={"google plus"} name="delete" selected = { this.state.current_mode_type === modeType.DELETE} onClick={(e) => this.onClickEvent(e)} />
                        } />
                        <Popup on={"hover"} content='Help' trigger={
                            <Button icon="help" color={"google plus"} name="help" onClick={(e) => this.onClickEvent(e)}>
                            </Button>
                        } />
                        <Popup on={"hover"} content='Normalize' trigger={
                            <Button icon="refresh" color={"google plus"} name="normalize" onClick={(e) => this.onClickEvent(e)} />
                        } />
                    </div>
                </div>

                <div id={"help"} { ...elementMargin }>
                    <Help explainText = { this.state.explainText }/>
                </div>

                <div id={"canvas"} { ...elementMarginCanvas }>
                    <ModalList modalOpen={this.state.popUpVisible} template={this.state} modalClose={() => {this.setState({popUpVisible: false});}}/>
                    <SidebarLeftPush visible={this.state.sideBarVisible} hide={this.state.simpleMode} currentMode={this.state.current_mode_type} />
                </div>

                <div id={"undercmd"} style = { defFunctionMenu }>
                    <ScaleSlider/>
                    <Button name="export" selected = { this.state.current_mode_type === modeType.EXPORT } onClick={(event) => this.onClickEvent(event)}>Export String</Button>
                    <Button name="import" selected = { this.state.current_mode_type === modeType.IMPORT } onClick={(event) => this.onClickEvent(event)}>Import String</Button>
                    {!(this.state.simpleMode) ?
                        <Button name="simple" style={{display: "none"}} onClick={(event) => this.onClickEvent(event)} >Simple mode</Button> :
                        <Button name="expert" style={{display: "none"}} onClick={(event) => this.onClickEvent(event)}>Expert mode</Button>
                    }
                    <Button style={{display: "none"}} name="custom" onClick={(event) => this.onClickEvent(event)}>Custom</Button>
                    <Button name="search" onClick={(event) => this.onClickEvent(event)}>Search</Button>
                </div>

                <div id={"modal"}>
                    <ModalCanvasDelete />
                </div>
            </div>
        );
    }

    initFunctions () {
        //TODO: if monosaccharide composition already depicted
        /*
        if(compositions.length !== 0) {
            liaise.initStage();
        }
         */
        liaise.initSelectedParams();
        document.getElementById("menu").style.display = "none";
    }

    changeVisual (_currentState, _targetId) {
        if (!_currentState.simpleMode) {
            _currentState.sideBarVisible =
                (!(_currentState.lastWork === _targetId && _currentState.sideBarVisible));
        } else {
            _currentState.popUpVisible = true;
        }
        if (!_currentState.sideBarVisible && !_currentState.simpleMode) {
            _currentState.current_mode_type = modeType.NOT_SELECTED;
        }

        return _currentState;
    }

    close () {
        let currentState = this.state;
        currentState.current_mode_type = modeType.NOT_SELECTED;
        this.setState(currentState);
    }

    clearCanvas() {
        liaise.initStage();
        liaise.initGraph();
        liaise.initSelectedParams();
        liaise.setNewShapes({});
        liaise.setNewTreeData({});
        this.close();
        document.querySelector("#menu").textContent = "";
        document.querySelector("#idtable").textContent = "";
        document.querySelector("#textArea").textContent = "";
    }

    addModeCancelButton () {
        const cancel = () => {
            let currentState = this.state;
            currentState.current_mode_type = modeType.NOT_SELECTED;
            currentState.sideBarVisible = false;
            currentState.popUpVisible = false;
            this.setState(currentState);
            liaise.initSelectedParams();
            document.getElementById("modecontent").lastChild.innerText = "";
            document.getElementById("modecancel").innerText = "";
        };

        // append mode cancel button
        ReactDOM.render(
            <ModeCancelButton onCancel={() => cancel()}/>,
            document.getElementById("modecancel")
        );
    }

    makeCancelUtility () {
        return () => {
            let currentState = this.state;
            currentState.current_mode_type = modeType.NOT_SELECTED;
            currentState.sideBarVisible = false;
            currentState.popUpVisible = false;
            this.setState(currentState);
        };
    }
}