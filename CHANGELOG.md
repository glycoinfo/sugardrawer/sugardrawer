# Change log

## 1.8.1 (2024/04/16)
* Updated API version

## 1.8.0 (2023/xx/xx)
* Updated API version

## 1.7.0 (2021/12/08)
* Updated the table of search results when searching GlycoNAVI

## 1.6.0 (2021/12/01)
* Fixed the monosaccharide name displayed when hovering the mouse cursor over the white monosaccharide symbol
* Fixed URL for GlycoNAVI-API
* Fixed the showing correct monosaccharide symbol when selecting the Deoxynonulosonate (white diamond)

## 1.5.0 (2021/10/14)
* Show a selecting monosaccharide or substituent near the mouse cursor
* Removed the Undefined section in the Add Monosaccharide menu
* Fixed zoom function
* Show a name of monosaccharide with pop-up window on the monosaccharide symbol
* Show a current scale with pop-up window

## 1.4.0 (2021/08/13)
* Changed search API
* Update user gide (see [README](README.md))

## 1.3.0 (2021/7/15)
* Removed sia from Monosaccharide List
* Update and fixed interface

## 1.2.0 (2021/7/13)
* Fixed an error with the backup generation process for the Undo/Redo function
* Added substituent (O-methyl)

## 1.1.0 (2021/7/12)
* Added substituent (glycolyl, iodo, n-dimethyl)
* Fixed typo of notation (thio, imino)
* Removed some substituents (nitrate, r/s-pyruvate, r/s-lactate)
  * The reason for the removal these substituents are described in [Restrictions](README.md#restrictions)

## 1.0.3 (2021/7/8)
* Added unsupported glycan templates
* Modified text components for edge and substituent
   
## 1.0.2 (2021/6/22)
* Added refresh and replace function
* Modified image components for monosaccharide and glycan fragments

## 1.0.1 (2021/6/21)
* Fixed incorrect  N-linked glycan templates. 
  * Hybrid and Complex

## 1.0.0 (2019/11/15)
* Developed interface