//@flow
"use strict";

export let basicData: Object = {
    "symbolSize": 15,
    "edgeSize": 1.0,
    "linkageSize": 11,
    "linkageTopYPositiom": -20,
    "linkageBottomYPosition": -5,
    "repeatRange": 10,
    "repeatStrokeSize": 5,
    "repeatNumberX": 10,
    "repeatNumberY": -10,
    "repeatNumberSize": 20,
    "cyclicEdge" : 150,    //cyclic構造のEdgeの曲がり具合
    "fragmentBracketToParentGlycan": 10,
    "fragmentBracketStrokeSize": 1,
    "symbolDistance": 45,
    "canvasSize": 5000,
    "fragmentEdgeDistance": 15,
    "commaUpperDistance": 15,
    "commaBottomDistance": 5,
    "modificationSize": 15,
    "compositionTextToSymbol": 10,
    "compositionTextSize": 30,
    "bridgeSize": 30,
    "isomerSize": 12,
    "ringSize": 12,
    "svgLinkageUp": 8,  //右、下にずらす
    "svgLinkagedown": -8,
    "strokeSize": 1
};