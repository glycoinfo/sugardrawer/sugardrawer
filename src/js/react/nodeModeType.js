"use strict";

export const nodeModeType = {
    NOT_SELECTED:Symbol(),
    HEX: Symbol(),
    GLC: Symbol(),
    MAN: Symbol(),
    GAL: Symbol(),
    GUL: Symbol(),
    ALT: Symbol(),
    ALL: Symbol(),
    TAL: Symbol(),
    IDO: Symbol(),
    HEXNAC: Symbol(),
    GLCNAC: Symbol(),
    MANNAC: Symbol(),
    GALNAC: Symbol(),
    GULNAC: Symbol(),
    ALTNAC: Symbol(),
    ALLNAC: Symbol(),
    TALNAC: Symbol(),
    IDONAC: Symbol(),
    HEXN: Symbol(),
    GLCN: Symbol(),
    MANN: Symbol(),
    GALN: Symbol(),
    GULN: Symbol(),
    ALTN: Symbol(),
    ALLN: Symbol(),
    TALN: Symbol(),
    IDON: Symbol(),
    HEXA: Symbol(),
    GLCA: Symbol(),
    MANA: Symbol(),
    GALA: Symbol(),
    GULA: Symbol(),
    ALTA: Symbol(),
    ALLA: Symbol(),
    TALA: Symbol(),
    IDOA: Symbol(),
    DHEX: Symbol(),
    QUI: Symbol(),
    RHA: Symbol(),
    D6GUL: Symbol(),
    D6ALT: Symbol(),
    D6TAL: Symbol(),
    FUC: Symbol(),
    DHEXNAC: Symbol(),
    QUINAC: Symbol(),
    RHANAC: Symbol(),
    D6ALTNAC: Symbol(),
    D6GULNAC: Symbol(),
    D6TALNAC: Symbol(),
    FUCNAC: Symbol(),
    DDHEX: Symbol(),
    OLI: Symbol(),
    TYV: Symbol(),
    ABE: Symbol(),
    PAR: Symbol(),
    DIG: Symbol(),
    COL: Symbol(),
    PEN: Symbol(),
    ARA: Symbol(),
    LYX: Symbol(),
    XYL: Symbol(),
    RIB: Symbol(),
    NON: Symbol(),
    KDN: Symbol(),
    NEU5AC: Symbol(),
    NEU5GC: Symbol(),
    NEU: Symbol(),
    SIA: Symbol(),
    DNON: Symbol(),
    PSE: Symbol(),
    LEG: Symbol(),
    ACI: Symbol(),
    E4LEG: Symbol(),
    UNKNOWN: Symbol(),
    BAC: Symbol(),
    LDMANHEP: Symbol(),
    KDO: Symbol(),
    DHA: Symbol(),
    DDMANHEP: Symbol(),
    MURNAC: Symbol(),
    MURNGC: Symbol(),
    MUR: Symbol(),
    ASSIGNED: Symbol(),
    API: Symbol(),
    FRU: Symbol(),
    TAG: Symbol(),
    SOR: Symbol(),
    PSI: Symbol()
};