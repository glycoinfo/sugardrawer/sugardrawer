let path = require("path");
let fileName = "js/main.js";
let src = path.resolve(__dirname, "src");
let dist = path.resolve(__dirname, "dist");
let HtmlWebpackPlugin = require("html-webpack-plugin");
const environment = process.env.NODE_ENV || "glytoucan";

module.exports = {
    entry: {
        app: path.join(src, fileName)
    },
    output: {
        path: dist,
        filename: "[name].bundle.js"
    },
    devtool: "source-map",
    module: {
        loaders: [
            {
                //BabelでJSコードをES2015+ -> ES5変換
                test: /\.js$/,
                exclude: /node_module | bower_components/,
                loader: "babel-loader"
            },
            {
                test: /\.css$/,
                loader: "style-loader!css"
            },
            {
                test: /\.(jpg|png)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[path][hash].[ext]",
                            outputPath: "/"
                        }
                    }
                ]
            },
            {
                //EaselJSの読み込み,
                test: require.resolve("createjs-easeljs"),
                loader: "imports-loader?this=>window!exports-loader?window.createjs"
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(src, "index.html"),
            filename: "index.html"
        })
    ],
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            APIConfig: path.join(__dirname, `/src/js/config/${environment}.js`)
        }
    }
};
