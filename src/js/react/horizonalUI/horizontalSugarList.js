//@flow

"use strict";

import React from "react";
import MonosaccharideContent from "../expertUI/MonosaccharideContent";
import {Popup} from "semantic-ui-react";

export const searchHorizontalSugarList = (_species: string): HTMLElement => {
    const sugarStyle: Object = {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "center",
        alignItems: "center",
        gap: "0px 1px"
    };

    switch(_species) {
        case "mammal": {
            return (
                <div id={"mammal"} style={sugarStyle} >
                    <MonosaccharideContent item={"Glc"} notation={"Glc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Man"} notation={"Man"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Gal"} notation={"Gal"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"GlcNAc"} notation={"GlcNAc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"GalNAc"} notation={"GalNAc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Fuc"} notation={"Fuc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Xyl"} notation={"Xyl"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Neu5Gc"} notation={"Neu5Gc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Neu5Ac"} notation={"Neu5Ac"} width={50} option={"shortcut"}/>
                </div>
            );
        }
        case "bacteria": {
            return (
                <div id={"bacteria"}>
                    B
                    A
                    C
                    T
                    E
                    R
                    I
                    A
                </div>
            );
        }
        case "plant": {
            return (
                <div id={"plant"}>
                    P
                    L
                    A
                    N
                    T
                    S
                </div>
            );
        }
        default: {
            return (
                <div id={"mammal"} style={sugarStyle} >
                    <MonosaccharideContent item={"Glc"} notation={"Glc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Man"} notation={"Man"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Gal"} notation={"Gal"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"GlcNAc"} notation={"GlcNAc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"GalNAc"} notation={"GalNAc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Fuc"} notation={"Fuc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Xyl"} notation={"Xyl"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Neu5Gc"} notation={"Neu5Gc"} width={50} option={"shortcut"}/>
                    <MonosaccharideContent item={"Neu5Ac"} notation={"Neu5Ac"} width={50} option={"shortcut"}/>
                </div>
            );
        }
    }
};

export const searchHorizontalNodeIndex = (_species: string): Array<String> => {
    switch (_species) {
        case "mammal": {
            return ["Glc", "Man", "Gal", "GlcNAc", "GalNAc", "Fuc", "Xyl", "Neu5Gc", "Neu5Ac"];
        }
        case "bacteria": {
            return [];
        }
        case "plant": {
            return [];
        }
        default: {
            return ["Glc", "Man", "Gal", "GlcNAc", "GalNAc", "Fuc", "Xyl", "Neu5Gc", "Neu5Ac"];
        }
    }
};