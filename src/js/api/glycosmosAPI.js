"use strict";

export const API = {
    wurcs2json: "https://api.glycosmos.org/glycanformatconverter/2.10.0/wurcs2wurcsjson/",
    wurcs2glycoct: "",
    glycoct2wurcs: "https://api.glycosmos.org/glycanformatconverter/2.10.0/glycoct2wurcs/",
    wurcsjson2svg: "https://api.test.glycosmos.org/wurcsjson2svg",
    wurcs2image: "https://api.glycosmos.org/wurcs2image/1.25.0/png/html/"
};

// wurcs2image: https://api.glycosmos.org/wurcs2image/0.8.0/png/{format}/{wurcs}
// format: html
